
<?php

use Faker\Generator as Faker;
use App\City;

$factory->define(App\Candidate::class, function (Faker $faker) {
    return [
    	'city_id' => factory(City::class),
        'name' => $faker->name,
        'date_birth' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'status' => 'Free',
    ];
});
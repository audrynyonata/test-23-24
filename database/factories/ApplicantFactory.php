<?php

use Faker\Generator as Faker;
use App\Candidate;
use App\Job;

$factory->define(App\Applicant::class, function (Faker $faker) {
    return [
    	'candidate_id' => factory(Candidate::class),
    	'job_id' => factory(Job::class),
    	'date' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'status' => $faker->randomElement($array = array ('Diterima', 'Ditolak')),
    ];
});

<?php

use Faker\Generator as Faker;
use App\Candidate;

$factory->define(App\Grade::class, function (Faker $faker) {
	$subjects = array ('Matematika', 'Bahasa Indonesia', 'Bahasa Inggris', 'Bahasa Mandarin', 'Bahasa Perancis', 'Bahasa Jerman', 'Fisika', 'Biologi', 'Kimia', 'Sejarah', 'Geografi', 'Ekonomi', 'Sosiologi', 'Akuntansi');

    return [
    	'subject' => $faker->randomElement($subjects),
        'mark' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = 100),
    ];
});

<?php

use Faker\Generator as Faker;
use App\Candidate;

$factory->define(App\Education::class, function (Faker $faker) {
    return [
    	'level' => $faker->randomElement($array = array ('TK', 'SD','SMP','SMA', 'SMK', 'D1', 'D2', 'D3', 'D4', 'S1', 'S2', 'S3')),
        'year' => $faker->year($max = 'now'),  
    ];
});

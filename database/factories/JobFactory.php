<?php

use Faker\Generator as Faker;
use App\Company;
use App\Position;

$factory->define(App\Job::class, function (Faker $faker) {
	$company = factory(Company::class)->create();
    $position = factory(Position::class)->create();
    return [
    	'company_id' => $company->id,
    	'position_id' => $position->id,
        'name' => 'Lowongan ' . $position->title . ' PT ' . $company->name,
        'quota' => $faker->numberBetween($min = 1, $max = 100),
        'description' => $faker->text($maxNbChars = 1000),
        'valid_date' => $faker->dateTimeInInterval($startDate = '-7 days', $interval = '+ 21 days', $timezone = null),
    ];
});

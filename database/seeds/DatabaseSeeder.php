<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        factory(App\Applicant::class, 25)->create();
        App\Candidate::get()->each(function($u){
            $u->education()->saveMany(factory(App\Education::class,2)->make());
            $u->grades()->saveMany(factory(App\Grade::class,3)->make());
        });

    }
}

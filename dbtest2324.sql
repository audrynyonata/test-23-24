-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: dbtest2324
-- ------------------------------------------------------
-- Server version	5.7.22-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `applicants`
--

DROP TABLE IF EXISTS `applicants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applicants` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `candidate_id` int(10) unsigned NOT NULL,
  `job_opening_id` int(10) unsigned NOT NULL,
  `date` date NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `applicants_candidate_id_index` (`candidate_id`),
  KEY `applicants_job_opening_id_index` (`job_opening_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applicants`
--

LOCK TABLES `applicants` WRITE;
/*!40000 ALTER TABLE `applicants` DISABLE KEYS */;
INSERT INTO `applicants` VALUES (1,1,1,'1973-11-20','Diterima','2018-05-27 22:15:59','2018-05-27 22:15:59'),(2,2,2,'2012-05-25','Ditolak','2018-05-27 22:15:59','2018-05-27 22:15:59'),(3,3,3,'2001-02-05','Ditolak','2018-05-27 22:15:59','2018-05-27 22:15:59'),(4,4,4,'1982-07-03','Diterima','2018-05-27 22:15:59','2018-05-27 22:15:59'),(5,5,5,'1995-09-06','Ditolak','2018-05-27 22:15:59','2018-05-27 22:15:59'),(6,6,6,'2008-08-14','Ditolak','2018-05-27 22:15:59','2018-05-27 22:15:59'),(7,7,7,'2009-04-24','Diterima','2018-05-27 22:15:59','2018-05-27 22:15:59'),(8,8,8,'2013-08-03','Diterima','2018-05-27 22:15:59','2018-05-27 22:15:59'),(9,9,9,'1985-06-06','Diterima','2018-05-27 22:15:59','2018-05-27 22:15:59'),(10,10,10,'2015-11-30','Diterima','2018-05-27 22:15:59','2018-05-27 22:15:59'),(11,11,11,'2001-03-27','Diterima','2018-05-27 22:15:59','2018-05-27 22:15:59'),(12,12,12,'1978-10-16','Diterima','2018-05-27 22:15:59','2018-05-27 22:15:59'),(13,13,13,'1970-12-17','Diterima','2018-05-27 22:16:00','2018-05-27 22:16:00'),(14,14,14,'2002-10-16','Ditolak','2018-05-27 22:16:00','2018-05-27 22:16:00'),(15,15,15,'1986-07-04','Ditolak','2018-05-27 22:16:00','2018-05-27 22:16:00'),(16,16,16,'1984-01-09','Diterima','2018-05-27 22:16:00','2018-05-27 22:16:00'),(17,17,17,'1992-07-19','Ditolak','2018-05-27 22:16:00','2018-05-27 22:16:00'),(18,18,18,'2002-11-24','Ditolak','2018-05-27 22:16:00','2018-05-27 22:16:00'),(19,19,19,'2006-07-23','Ditolak','2018-05-27 22:16:00','2018-05-27 22:16:00'),(20,20,20,'2014-09-23','Diterima','2018-05-27 22:16:00','2018-05-27 22:16:00'),(21,21,21,'1992-12-12','Diterima','2018-05-27 22:16:00','2018-05-27 22:16:00'),(22,22,22,'2001-08-15','Diterima','2018-05-27 22:16:00','2018-05-27 22:16:00'),(23,23,23,'2000-01-22','Diterima','2018-05-27 22:16:00','2018-05-27 22:16:00'),(24,24,24,'1990-12-02','Ditolak','2018-05-27 22:16:00','2018-05-27 22:16:00'),(25,25,25,'1999-11-18','Diterima','2018-05-27 22:16:00','2018-05-27 22:16:00');
/*!40000 ALTER TABLE `applicants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidates`
--

DROP TABLE IF EXISTS `candidates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `city_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` int(11) NOT NULL,
  `birth_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `candidates_city_id_index` (`city_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidates`
--

LOCK TABLES `candidates` WRITE;
/*!40000 ALTER TABLE `candidates` DISABLE KEYS */;
INSERT INTO `candidates` VALUES (1,1,'Daisy Jerde PhD',2,'2015-12-02','2018-05-27 22:15:43','2018-05-27 22:15:43'),(2,2,'Maxine Olson',27,'1990-08-01','2018-05-27 22:15:44','2018-05-27 22:15:44'),(3,3,'Cordia Rau III',26,'1991-08-18','2018-05-27 22:15:44','2018-05-27 22:15:44'),(4,4,'Prof. Arno Parker',38,'1980-05-21','2018-05-27 22:15:45','2018-05-27 22:15:45'),(5,5,'Dr. Dana Auer IV',14,'2003-08-09','2018-05-27 22:15:47','2018-05-27 22:15:47'),(6,6,'Darrion Grant',41,'1976-06-26','2018-05-27 22:15:49','2018-05-27 22:15:49'),(7,7,'Naomi Pouros',2,'2015-11-02','2018-05-27 22:15:49','2018-05-27 22:15:49'),(8,8,'Sandra Maggio',25,'1992-06-23','2018-05-27 22:15:50','2018-05-27 22:15:50'),(9,9,'Miss Leatha Lubowitz',25,'1992-07-11','2018-05-27 22:15:50','2018-05-27 22:15:50'),(10,10,'Lisandro Auer',1,'2016-08-02','2018-05-27 22:15:51','2018-05-27 22:15:51'),(11,11,'Zane Ziemann',45,'1973-03-30','2018-05-27 22:15:51','2018-05-27 22:15:51'),(12,12,'Julianne Weimann',43,'1975-04-05','2018-05-27 22:15:52','2018-05-27 22:15:52'),(13,13,'Donavon Cartwright',46,'1971-07-28','2018-05-27 22:15:53','2018-05-27 22:15:53'),(14,14,'Brenda Heaney',2,'2015-06-24','2018-05-27 22:15:53','2018-05-27 22:15:53'),(15,15,'Theresia Larkin',9,'2008-06-21','2018-05-27 22:15:54','2018-05-27 22:15:54'),(16,16,'Mr. Justen Fay',42,'1975-10-18','2018-05-27 22:15:55','2018-05-27 22:15:55'),(17,17,'Theodora Runolfsson III',7,'2011-02-02','2018-05-27 22:15:55','2018-05-27 22:15:55'),(18,18,'Dr. Graciela Brown',10,'2007-10-23','2018-05-27 22:15:56','2018-05-27 22:15:56'),(19,19,'Odie Lindgren',45,'1972-09-09','2018-05-27 22:15:56','2018-05-27 22:15:56'),(20,20,'Alia Abbott',35,'1982-11-23','2018-05-27 22:15:57','2018-05-27 22:15:57'),(21,21,'Ms. Daniella Roob III',34,'1984-02-19','2018-05-27 22:15:57','2018-05-27 22:15:57'),(22,22,'Prof. Lonie Kulas',12,'2005-11-05','2018-05-27 22:15:57','2018-05-27 22:15:57'),(23,23,'Armani Vandervort',17,'2001-03-04','2018-05-27 22:15:58','2018-05-27 22:15:58'),(24,24,'Vita Witting I',36,'1981-09-07','2018-05-27 22:15:58','2018-05-27 22:15:58'),(25,25,'Morton Abbott',36,'1982-02-03','2018-05-27 22:15:58','2018-05-27 22:15:58');
/*!40000 ALTER TABLE `candidates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities`
--

LOCK TABLES `cities` WRITE;
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
INSERT INTO `cities` VALUES (1,'Port Maud','2018-05-27 22:15:43','2018-05-27 22:15:43'),(2,'West Vinnieville','2018-05-27 22:15:44','2018-05-27 22:15:44'),(3,'Uptonland','2018-05-27 22:15:44','2018-05-27 22:15:44'),(4,'New Reva','2018-05-27 22:15:45','2018-05-27 22:15:45'),(5,'Joanaton','2018-05-27 22:15:46','2018-05-27 22:15:46'),(6,'Oberbrunnerchester','2018-05-27 22:15:49','2018-05-27 22:15:49'),(7,'Lake Daron','2018-05-27 22:15:49','2018-05-27 22:15:49'),(8,'East Barrymouth','2018-05-27 22:15:49','2018-05-27 22:15:49'),(9,'Kristinburgh','2018-05-27 22:15:50','2018-05-27 22:15:50'),(10,'Port Carole','2018-05-27 22:15:51','2018-05-27 22:15:51'),(11,'Lake Gillianfort','2018-05-27 22:15:51','2018-05-27 22:15:51'),(12,'East Asa','2018-05-27 22:15:52','2018-05-27 22:15:52'),(13,'Nikolausside','2018-05-27 22:15:53','2018-05-27 22:15:53'),(14,'Lake Elinorehaven','2018-05-27 22:15:53','2018-05-27 22:15:53'),(15,'Lake Ivory','2018-05-27 22:15:54','2018-05-27 22:15:54'),(16,'Jonesmouth','2018-05-27 22:15:55','2018-05-27 22:15:55'),(17,'South Annettashire','2018-05-27 22:15:55','2018-05-27 22:15:55'),(18,'Alanborough','2018-05-27 22:15:56','2018-05-27 22:15:56'),(19,'Akeemland','2018-05-27 22:15:56','2018-05-27 22:15:56'),(20,'Port Genoveva','2018-05-27 22:15:57','2018-05-27 22:15:57'),(21,'New Gildamouth','2018-05-27 22:15:57','2018-05-27 22:15:57'),(22,'Lefflerview','2018-05-27 22:15:57','2018-05-27 22:15:57'),(23,'Modestaview','2018-05-27 22:15:58','2018-05-27 22:15:58'),(24,'Emileland','2018-05-27 22:15:58','2018-05-27 22:15:58'),(25,'East Emilioberg','2018-05-27 22:15:58','2018-05-27 22:15:58');
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companies`
--

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` VALUES (1,'Bode Ltd','Bode Ltd Inc','2018-05-27 22:15:43','2018-05-27 22:15:43'),(2,'Weissnat, Schmitt and Jaskolski','Weissnat, Schmitt and Jaskolski and Sons','2018-05-27 22:15:44','2018-05-27 22:15:44'),(3,'Monahan-Okuneva','Monahan-Okuneva Inc','2018-05-27 22:15:44','2018-05-27 22:15:44'),(4,'Lindgren-Hansen','Lindgren-Hansen Group','2018-05-27 22:15:45','2018-05-27 22:15:45'),(5,'Prohaska, Bode and Prosacco','Prohaska, Bode and Prosacco and Sons','2018-05-27 22:15:47','2018-05-27 22:15:47'),(6,'Nolan-Okuneva','Nolan-Okuneva Inc','2018-05-27 22:15:49','2018-05-27 22:15:49'),(7,'Kilback LLC','Kilback LLC and Sons','2018-05-27 22:15:49','2018-05-27 22:15:49'),(8,'Witting, Aufderhar and Stanton','Witting, Aufderhar and Stanton Group','2018-05-27 22:15:50','2018-05-27 22:15:50'),(9,'Lesch-Walker','Lesch-Walker LLC','2018-05-27 22:15:51','2018-05-27 22:15:51'),(10,'Boyer-Mills','Boyer-Mills LLC','2018-05-27 22:15:51','2018-05-27 22:15:51'),(11,'Quitzon LLC','Quitzon LLC PLC','2018-05-27 22:15:52','2018-05-27 22:15:52'),(12,'Hettinger, Block and Harber','Hettinger, Block and Harber Inc','2018-05-27 22:15:52','2018-05-27 22:15:52'),(13,'Pfeffer-Beatty','Pfeffer-Beatty and Sons','2018-05-27 22:15:53','2018-05-27 22:15:53'),(14,'Stamm-Huel','Stamm-Huel Ltd','2018-05-27 22:15:53','2018-05-27 22:15:53'),(15,'Kerluke PLC','Kerluke PLC LLC','2018-05-27 22:15:54','2018-05-27 22:15:54'),(16,'Kris, Rice and Nienow','Kris, Rice and Nienow and Sons','2018-05-27 22:15:55','2018-05-27 22:15:55'),(17,'Langosh-Veum','Langosh-Veum PLC','2018-05-27 22:15:55','2018-05-27 22:15:55'),(18,'Gottlieb Group','Gottlieb Group Group','2018-05-27 22:15:56','2018-05-27 22:15:56'),(19,'Cronin, Stracke and Hamill','Cronin, Stracke and Hamill Inc','2018-05-27 22:15:56','2018-05-27 22:15:56'),(20,'O\'Connell LLC','O\'Connell LLC LLC','2018-05-27 22:15:57','2018-05-27 22:15:57'),(21,'Wyman, Schroeder and Nicolas','Wyman, Schroeder and Nicolas and Sons','2018-05-27 22:15:57','2018-05-27 22:15:57'),(22,'Runolfsson Inc','Runolfsson Inc Group','2018-05-27 22:15:58','2018-05-27 22:15:58'),(23,'Fritsch-Sauer','Fritsch-Sauer Ltd','2018-05-27 22:15:58','2018-05-27 22:15:58'),(24,'Pfannerstill-O\'Conner','Pfannerstill-O\'Conner Ltd','2018-05-27 22:15:58','2018-05-27 22:15:58'),(25,'Rohan Ltd','Rohan Ltd Inc','2018-05-27 22:15:58','2018-05-27 22:15:58');
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `education`
--

DROP TABLE IF EXISTS `education`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `education` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `candidate_id` int(10) unsigned NOT NULL,
  `level` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `year` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `education_candidate_id_index` (`candidate_id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `education`
--

LOCK TABLES `education` WRITE;
/*!40000 ALTER TABLE `education` DISABLE KEYS */;
INSERT INTO `education` VALUES (1,1,'TK',1971,'2018-05-27 22:16:01','2018-05-27 22:16:01'),(2,1,'SD',1998,'2018-05-27 22:16:01','2018-05-27 22:16:01'),(3,2,'SMK',2003,'2018-05-27 22:16:01','2018-05-27 22:16:01'),(4,2,'D4',1987,'2018-05-27 22:16:01','2018-05-27 22:16:01'),(5,3,'SMA',1979,'2018-05-27 22:16:01','2018-05-27 22:16:01'),(6,3,'SD',1979,'2018-05-27 22:16:02','2018-05-27 22:16:02'),(7,4,'SMP',1974,'2018-05-27 22:16:02','2018-05-27 22:16:02'),(8,4,'S1',2016,'2018-05-27 22:16:02','2018-05-27 22:16:02'),(9,5,'S2',1985,'2018-05-27 22:16:03','2018-05-27 22:16:03'),(10,5,'S2',1987,'2018-05-27 22:16:03','2018-05-27 22:16:03'),(11,6,'SD',1991,'2018-05-27 22:16:03','2018-05-27 22:16:03'),(12,6,'D1',1989,'2018-05-27 22:16:03','2018-05-27 22:16:03'),(13,7,'D4',1983,'2018-05-27 22:16:03','2018-05-27 22:16:03'),(14,7,'S2',1970,'2018-05-27 22:16:03','2018-05-27 22:16:03'),(15,8,'S1',2002,'2018-05-27 22:16:04','2018-05-27 22:16:04'),(16,8,'D2',1991,'2018-05-27 22:16:04','2018-05-27 22:16:04'),(17,9,'SMA',2012,'2018-05-27 22:16:04','2018-05-27 22:16:04'),(18,9,'S2',2004,'2018-05-27 22:16:04','2018-05-27 22:16:04'),(19,10,'S3',1994,'2018-05-27 22:16:05','2018-05-27 22:16:05'),(20,10,'D2',1981,'2018-05-27 22:16:05','2018-05-27 22:16:05'),(21,11,'D2',2013,'2018-05-27 22:16:05','2018-05-27 22:16:05'),(22,11,'D1',1991,'2018-05-27 22:16:05','2018-05-27 22:16:05'),(23,12,'SMA',1971,'2018-05-27 22:16:06','2018-05-27 22:16:06'),(24,12,'D3',2012,'2018-05-27 22:16:06','2018-05-27 22:16:06'),(25,13,'S1',2018,'2018-05-27 22:16:06','2018-05-27 22:16:06'),(26,13,'S3',2018,'2018-05-27 22:16:06','2018-05-27 22:16:06'),(27,14,'D2',2008,'2018-05-27 22:16:07','2018-05-27 22:16:07'),(28,14,'D3',1977,'2018-05-27 22:16:07','2018-05-27 22:16:07'),(29,15,'S1',1994,'2018-05-27 22:16:07','2018-05-27 22:16:07'),(30,15,'S2',1982,'2018-05-27 22:16:08','2018-05-27 22:16:08'),(31,16,'SMP',2018,'2018-05-27 22:16:08','2018-05-27 22:16:08'),(32,16,'S1',2017,'2018-05-27 22:16:08','2018-05-27 22:16:08'),(33,17,'D2',1993,'2018-05-27 22:16:08','2018-05-27 22:16:08'),(34,17,'S3',1980,'2018-05-27 22:16:08','2018-05-27 22:16:08'),(35,18,'D4',1972,'2018-05-27 22:16:09','2018-05-27 22:16:09'),(36,18,'D4',1976,'2018-05-27 22:16:09','2018-05-27 22:16:09'),(37,19,'TK',1987,'2018-05-27 22:16:09','2018-05-27 22:16:09'),(38,19,'S3',1975,'2018-05-27 22:16:09','2018-05-27 22:16:09'),(39,20,'D1',1986,'2018-05-27 22:16:10','2018-05-27 22:16:10'),(40,20,'SMK',2002,'2018-05-27 22:16:10','2018-05-27 22:16:10'),(41,21,'S2',2006,'2018-05-27 22:16:10','2018-05-27 22:16:10'),(42,21,'D2',1971,'2018-05-27 22:16:10','2018-05-27 22:16:10'),(43,22,'TK',1981,'2018-05-27 22:16:11','2018-05-27 22:16:11'),(44,22,'D4',2001,'2018-05-27 22:16:11','2018-05-27 22:16:11'),(45,23,'S2',1985,'2018-05-27 22:16:11','2018-05-27 22:16:11'),(46,23,'S2',1972,'2018-05-27 22:16:12','2018-05-27 22:16:12'),(47,24,'D2',1975,'2018-05-27 22:16:12','2018-05-27 22:16:12'),(48,24,'SMK',2017,'2018-05-27 22:16:12','2018-05-27 22:16:12'),(49,25,'D3',2017,'2018-05-27 22:16:12','2018-05-27 22:16:12'),(50,25,'SMP',2012,'2018-05-27 22:16:12','2018-05-27 22:16:12');
/*!40000 ALTER TABLE `education` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grades`
--

DROP TABLE IF EXISTS `grades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grades` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `candidate_id` int(10) unsigned NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mark` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `grades_candidate_id_index` (`candidate_id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grades`
--

LOCK TABLES `grades` WRITE;
/*!40000 ALTER TABLE `grades` DISABLE KEYS */;
INSERT INTO `grades` VALUES (1,1,'Bahasa Jerman','81.63481','2018-05-27 22:16:01','2018-05-27 22:16:01'),(2,1,'Ekonomi','69.687689594','2018-05-27 22:16:01','2018-05-27 22:16:01'),(3,1,'Matematika','63.2671583','2018-05-27 22:16:01','2018-05-27 22:16:01'),(4,2,'Geografi','65.37','2018-05-27 22:16:01','2018-05-27 22:16:01'),(5,2,'Sejarah','36.7','2018-05-27 22:16:01','2018-05-27 22:16:01'),(6,2,'Bahasa Perancis','30.6','2018-05-27 22:16:01','2018-05-27 22:16:01'),(7,3,'Bahasa Inggris','59.613413','2018-05-27 22:16:02','2018-05-27 22:16:02'),(8,3,'Sejarah','63.059498','2018-05-27 22:16:02','2018-05-27 22:16:02'),(9,3,'Akuntansi','50.925','2018-05-27 22:16:02','2018-05-27 22:16:02'),(10,4,'Kimia','69.147554538','2018-05-27 22:16:02','2018-05-27 22:16:02'),(11,4,'Sosiologi','2','2018-05-27 22:16:02','2018-05-27 22:16:02'),(12,4,'Bahasa Jerman','58.56','2018-05-27 22:16:02','2018-05-27 22:16:02'),(13,5,'Fisika','51.901955','2018-05-27 22:16:03','2018-05-27 22:16:03'),(14,5,'Biologi','29.9330104','2018-05-27 22:16:03','2018-05-27 22:16:03'),(15,5,'Akuntansi','44.0451','2018-05-27 22:16:03','2018-05-27 22:16:03'),(16,6,'Bahasa Mandarin','3.5','2018-05-27 22:16:03','2018-05-27 22:16:03'),(17,6,'Biologi','23','2018-05-27 22:16:03','2018-05-27 22:16:03'),(18,6,'Bahasa Jerman','73.015475028','2018-05-27 22:16:03','2018-05-27 22:16:03'),(19,7,'Sejarah','91.4794','2018-05-27 22:16:03','2018-05-27 22:16:03'),(20,7,'Bahasa Indonesia','45.9853','2018-05-27 22:16:04','2018-05-27 22:16:04'),(21,7,'Sejarah','38.4','2018-05-27 22:16:04','2018-05-27 22:16:04'),(22,8,'Fisika','19.54836','2018-05-27 22:16:04','2018-05-27 22:16:04'),(23,8,'Bahasa Jerman','52.4','2018-05-27 22:16:04','2018-05-27 22:16:04'),(24,8,'Akuntansi','25','2018-05-27 22:16:04','2018-05-27 22:16:04'),(25,9,'Matematika','92.325520652','2018-05-27 22:16:04','2018-05-27 22:16:04'),(26,9,'Bahasa Perancis','78.641459662','2018-05-27 22:16:05','2018-05-27 22:16:05'),(27,9,'Sosiologi','47.985551','2018-05-27 22:16:05','2018-05-27 22:16:05'),(28,10,'Fisika','97','2018-05-27 22:16:05','2018-05-27 22:16:05'),(29,10,'Bahasa Indonesia','29.8292','2018-05-27 22:16:05','2018-05-27 22:16:05'),(30,10,'Geografi','12.21','2018-05-27 22:16:05','2018-05-27 22:16:05'),(31,11,'Ekonomi','46','2018-05-27 22:16:05','2018-05-27 22:16:05'),(32,11,'Bahasa Mandarin','33.41921','2018-05-27 22:16:05','2018-05-27 22:16:05'),(33,11,'Biologi','17','2018-05-27 22:16:06','2018-05-27 22:16:06'),(34,12,'Sejarah','53.435','2018-05-27 22:16:06','2018-05-27 22:16:06'),(35,12,'Bahasa Inggris','13.91','2018-05-27 22:16:06','2018-05-27 22:16:06'),(36,12,'Geografi','14.087','2018-05-27 22:16:06','2018-05-27 22:16:06'),(37,13,'Bahasa Perancis','45.913903','2018-05-27 22:16:06','2018-05-27 22:16:06'),(38,13,'Sosiologi','71','2018-05-27 22:16:06','2018-05-27 22:16:06'),(39,13,'Ekonomi','61.4421','2018-05-27 22:16:06','2018-05-27 22:16:06'),(40,14,'Fisika','11.6','2018-05-27 22:16:07','2018-05-27 22:16:07'),(41,14,'Fisika','59.16','2018-05-27 22:16:07','2018-05-27 22:16:07'),(42,14,'Matematika','93.0452311','2018-05-27 22:16:07','2018-05-27 22:16:07'),(43,15,'Bahasa Perancis','3.61954761','2018-05-27 22:16:08','2018-05-27 22:16:08'),(44,15,'Bahasa Inggris','38.93','2018-05-27 22:16:08','2018-05-27 22:16:08'),(45,15,'Bahasa Inggris','45.85','2018-05-27 22:16:08','2018-05-27 22:16:08'),(46,16,'Sosiologi','74.9','2018-05-27 22:16:08','2018-05-27 22:16:08'),(47,16,'Kimia','52.486555629','2018-05-27 22:16:08','2018-05-27 22:16:08'),(48,16,'Sosiologi','83.11666','2018-05-27 22:16:08','2018-05-27 22:16:08'),(49,17,'Sejarah','61.126753437','2018-05-27 22:16:08','2018-05-27 22:16:08'),(50,17,'Bahasa Indonesia','98.032156','2018-05-27 22:16:08','2018-05-27 22:16:08'),(51,17,'Bahasa Mandarin','43','2018-05-27 22:16:09','2018-05-27 22:16:09'),(52,18,'Sosiologi','57.00708141','2018-05-27 22:16:09','2018-05-27 22:16:09'),(53,18,'Bahasa Mandarin','19.4028','2018-05-27 22:16:09','2018-05-27 22:16:09'),(54,18,'Sosiologi','37.57273105','2018-05-27 22:16:09','2018-05-27 22:16:09'),(55,19,'Akuntansi','33.3846144','2018-05-27 22:16:10','2018-05-27 22:16:10'),(56,19,'Bahasa Inggris','41.5766397','2018-05-27 22:16:10','2018-05-27 22:16:10'),(57,19,'Ekonomi','65.8','2018-05-27 22:16:10','2018-05-27 22:16:10'),(58,20,'Biologi','49.7522','2018-05-27 22:16:10','2018-05-27 22:16:10'),(59,20,'Sosiologi','43.282547241','2018-05-27 22:16:10','2018-05-27 22:16:10'),(60,20,'Geografi','80.10744791','2018-05-27 22:16:10','2018-05-27 22:16:10'),(61,21,'Bahasa Indonesia','7.065','2018-05-27 22:16:10','2018-05-27 22:16:10'),(62,21,'Fisika','27','2018-05-27 22:16:10','2018-05-27 22:16:10'),(63,21,'Fisika','92.87','2018-05-27 22:16:11','2018-05-27 22:16:11'),(64,22,'Bahasa Indonesia','98.158','2018-05-27 22:16:11','2018-05-27 22:16:11'),(65,22,'Sosiologi','47.79','2018-05-27 22:16:11','2018-05-27 22:16:11'),(66,22,'Sejarah','69.975004331','2018-05-27 22:16:11','2018-05-27 22:16:11'),(67,23,'Ekonomi','13.99529','2018-05-27 22:16:12','2018-05-27 22:16:12'),(68,23,'Bahasa Perancis','72.53288','2018-05-27 22:16:12','2018-05-27 22:16:12'),(69,23,'Bahasa Inggris','25.023','2018-05-27 22:16:12','2018-05-27 22:16:12'),(70,24,'Matematika','40.8','2018-05-27 22:16:12','2018-05-27 22:16:12'),(71,24,'Geografi','64.3966605','2018-05-27 22:16:12','2018-05-27 22:16:12'),(72,24,'Bahasa Indonesia','76.927825','2018-05-27 22:16:12','2018-05-27 22:16:12'),(73,25,'Biologi','12.30548258','2018-05-27 22:16:12','2018-05-27 22:16:12'),(74,25,'Bahasa Inggris','27.372','2018-05-27 22:16:12','2018-05-27 22:16:12'),(75,25,'Akuntansi','65.62320309','2018-05-27 22:16:12','2018-05-27 22:16:12');
/*!40000 ALTER TABLE `grades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_openings`
--

DROP TABLE IF EXISTS `job_openings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_openings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(10) unsigned NOT NULL,
  `position_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quota` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `job_openings_company_id_index` (`company_id`),
  KEY `job_openings_position_id_index` (`position_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_openings`
--

LOCK TABLES `job_openings` WRITE;
/*!40000 ALTER TABLE `job_openings` DISABLE KEYS */;
INSERT INTO `job_openings` VALUES (1,1,1,'Lowongan Product Promoter PT Bode Ltd',65,'Ea qui eum cupiditate. Ut delectus sunt laudantium alias atque. Voluptas aut voluptas ipsum asperiores cupiditate enim. Voluptatem facere aut aut aut. Nihil nihil mollitia nobis consequatur ut. Consequuntur vitae omnis consectetur eos voluptatem cumque perferendis. Animi eum qui error iste voluptatibus possimus. Quia recusandae consequatur temporibus est quia voluptatem corrupti. Deserunt veritatis earum asperiores porro. At ut animi quis. Ut omnis dolor corporis deleniti vel fugiat beatae. Ut qui vero dolorem nihil ad necessitatibus. Culpa fugit possimus vero. Tenetur ut ab magni modi incidunt voluptatum quidem. Voluptatem distinctio error blanditiis cumque occaecati rem. Magnam adipisci dolores nihil id doloribus voluptatibus. Esse ex ea veritatis est deserunt repudiandae. Quibusdam repudiandae iusto perferendis debitis dolorum consequatur ea. Eligendi sequi qui autem debitis. Dolor corrupti temporibus at quas.','2018-05-27 22:15:44','2018-05-27 22:15:44'),(2,2,2,'Lowongan Movie Director oR Theatre Director PT Weissnat, Schmitt and Jaskolski',40,'Nisi harum omnis est mollitia laborum quia suscipit. Possimus aspernatur qui enim. Repellat id minus amet ea. Sit et asperiores sint aut est. Sit libero aliquam magni quos iure. Ratione error inventore nesciunt quidem nisi deserunt. Molestiae sed sed enim sint ullam nesciunt. Eum aliquam ut unde veritatis sint. Nesciunt corporis et ut sed voluptates sunt labore. Et aut quo repudiandae itaque. Nobis et necessitatibus vitae illum. Repellendus voluptatem error dolore enim eum commodi iste. Architecto adipisci inventore cumque possimus et natus. Quam aliquam ut expedita nostrum nihil. Voluptatem similique reiciendis quidem expedita magnam laboriosam. Ad veritatis veritatis non illo voluptatem facilis in. Consequatur beatae rem maiores ab architecto quia. Earum eius est quo necessitatibus. Et eius aliquid voluptatem et et. Dolorem dolorem dolore dolor id dolore assumenda cupiditate. Dolorem optio sunt reprehenderit fugit voluptas. Praesentium in dolorem totam.','2018-05-27 22:15:44','2018-05-27 22:15:44'),(3,3,3,'Lowongan Power Generating Plant Operator PT Monahan-Okuneva',66,'Iste non quas ipsam nam. Assumenda est et et nobis voluptas officia ut. Est dicta deserunt veritatis distinctio voluptatem et. Dolor voluptas praesentium et deleniti nihil mollitia architecto. Et totam doloremque asperiores ipsam voluptatem voluptatum tempore corrupti. Cupiditate nam ullam dolorum illo suscipit. Voluptatum tempora autem porro voluptatem quia suscipit occaecati. Dolore quo placeat modi iusto repellendus. Rerum omnis debitis est ab est. Dolore ea officiis rerum fugiat fugit. Qui ea harum qui eum cumque voluptas eos minus. Et accusamus aliquam fugiat qui error excepturi aut. Itaque perferendis consequatur nihil voluptates magni voluptas. Et natus quia iste tenetur quaerat tenetur maxime ut. Autem sit voluptatem possimus maxime id beatae voluptate. Possimus nobis voluptatem praesentium voluptatibus. Perspiciatis rem illo est voluptatem quaerat maiores. Deleniti eos et qui nam recusandae et tempore. Quae cumque sint veritatis aliquid.','2018-05-27 22:15:44','2018-05-27 22:15:44'),(4,4,4,'Lowongan Computer-Controlled Machine Tool Operator PT Lindgren-Hansen',85,'Quia eum consequuntur quos exercitationem quia ipsum. Animi soluta odit quia est eaque. Minus mollitia explicabo aut et ullam consequatur magni. Dolores minima perspiciatis atque. Porro aut quas officiis quod eos eos. Ratione rerum quibusdam eaque illum voluptatem. Molestiae et omnis commodi ut excepturi. Dolore explicabo sunt quibusdam quod et consequatur eum. Quasi magni suscipit nam placeat animi dignissimos enim. Culpa voluptatem eos id rerum non corporis ab sunt. Cum quia omnis magnam reprehenderit sapiente. Et saepe iure ea. Ratione provident voluptates repellendus eos enim eum. Sit recusandae eum nulla. Optio voluptates non et laudantium commodi et. Eum error minus odio maiores. Sint cum nemo qui unde similique qui in ipsum. Illo mollitia minima ut dolorem minima. Et deserunt accusamus quam dolorum. Temporibus qui reiciendis consequuntur ea veritatis aut. Et est mollitia voluptates quia voluptas eligendi. Et ab qui similique quis a placeat.','2018-05-27 22:15:46','2018-05-27 22:15:46'),(5,5,5,'Lowongan Cartographer PT Prohaska, Bode and Prosacco',37,'Inventore aut odit corrupti cumque ab architecto. Et quis quibusdam dolor autem. Sequi delectus consequatur rerum similique occaecati. Illum tempora laborum quae dolorum. Aliquam et eos fugiat aspernatur ipsam id sint. Voluptatibus eos voluptas voluptatem hic occaecati minima dolores. Voluptatibus est voluptatem fugit. Velit doloribus iste ipsa asperiores ea voluptates earum. Deserunt quis unde incidunt expedita a. Saepe quas voluptatem provident voluptas voluptas omnis incidunt. Sed et suscipit et quibusdam. Delectus beatae qui qui possimus sit. Voluptatibus repellendus vel nesciunt reiciendis. Illo iusto ullam officia aut. Commodi beatae minus vel quia corporis ut est. Omnis quia ea consequatur est inventore est tempore. Iusto numquam dolorum voluptatem aut quo. Sunt pariatur dolores et possimus soluta dolores eos. Eius eius voluptas magnam est. Iusto facere ea nihil voluptas.','2018-05-27 22:15:48','2018-05-27 22:15:48'),(6,6,6,'Lowongan Food Servers PT Nolan-Okuneva',33,'Sit eius praesentium molestias tempore. Dolores ut cumque quae accusamus libero id deserunt. Illum expedita vero illum quae veniam. Ea quo et omnis voluptate aliquid facilis ullam. At aut quis ex dolores. Temporibus sunt at qui dolores fuga. Provident saepe quo deleniti ut magni molestias impedit deleniti. Nobis nihil dolorem in repellendus voluptatem. Repellat et quia deserunt aperiam architecto. Eum quasi est id quibusdam et. Omnis nesciunt minima architecto tenetur. Porro cumque voluptas repudiandae eos accusamus velit quia. Sint assumenda magnam fugiat dolores nobis et minima. Quos mollitia veritatis omnis sit pariatur accusantium autem enim. Doloribus nulla dolores dolore quas. Labore ut quia voluptatem rerum omnis a. Non molestiae eligendi non facere architecto. Distinctio aliquam veritatis occaecati labore itaque animi.','2018-05-27 22:15:49','2018-05-27 22:15:49'),(7,7,7,'Lowongan Sales and Related Workers PT Kilback LLC',34,'Illo voluptatem et neque est. Optio neque doloremque nulla sunt id voluptas. Velit sapiente inventore voluptatem consequuntur recusandae quis quia. Qui voluptas eaque nihil asperiores possimus vero. Vero quo itaque minus vel. Necessitatibus nobis tenetur fuga rerum. Est magni ea sunt nobis nihil tempore corporis. Occaecati autem vitae magnam. Autem velit repellendus dignissimos ratione cum ullam voluptatem. Voluptatibus dolorum sint officiis consectetur unde quasi quo quae. Rerum iusto delectus voluptas fugit ipsum et ut. Ut itaque aliquam eveniet laboriosam a voluptatem eum dolorem. Consequatur sunt corrupti voluptas quam et. Fuga vero facere sit voluptatibus. Voluptas et quae error maxime. Ipsam magni quas ut non. Necessitatibus explicabo quod molestiae dolorem. Reiciendis aut rem minus odit cupiditate quibusdam. Minus explicabo consequuntur pariatur aut et quae quo autem.','2018-05-27 22:15:49','2018-05-27 22:15:49'),(8,8,8,'Lowongan Shear Machine Set-Up Operator PT Witting, Aufderhar and Stanton',83,'Recusandae omnis autem est aut unde aspernatur. Et culpa delectus aspernatur libero minima quaerat deserunt. Aut ut et sequi amet dolorum. Et soluta perferendis voluptatem sint nam mollitia et. Ex repudiandae corporis id laboriosam neque. Nesciunt id rem provident laborum tempore. Distinctio quia iure unde tempora dolorem molestiae non. Consequatur facilis minus distinctio molestiae sunt et. Dolor consequatur similique corporis in dolores. Voluptate qui est suscipit omnis recusandae. Cum sed quisquam asperiores. Corrupti quisquam nesciunt qui vel dignissimos quos. Vero culpa consequuntur commodi ut. Laboriosam laboriosam est commodi nesciunt veritatis. Occaecati voluptas quidem ullam non in expedita et. Est recusandae aliquam illo omnis est laboriosam. Iure sapiente qui labore autem modi et. Laudantium porro ipsum consequuntur et. Consequatur qui adipisci consequatur quo dolor quisquam.','2018-05-27 22:15:50','2018-05-27 22:15:50'),(9,9,9,'Lowongan Waste Treatment Plant Operator PT Lesch-Walker',54,'Nobis architecto facilis voluptates qui autem. Vel similique atque molestiae dolores. Molestias et provident aut. Sint ipsum in illum expedita numquam nemo et. Et deleniti amet excepturi quisquam ut. Voluptas reprehenderit porro aliquid accusamus fugiat qui odit. Atque nihil illo ex quo dolore laborum incidunt. Doloribus quaerat et est aspernatur. Libero fugit natus neque corporis reprehenderit sunt sunt. Perspiciatis aliquam soluta et architecto eos. Ab aut aut natus. Odit exercitationem vitae id aliquid est qui vel qui. Magnam eum quos dolorum possimus rem explicabo. Et fugiat omnis quo ullam. Ea quis velit aut error asperiores ut. Vitae quas possimus est sit quo. Ipsum nam in facilis dolor. Eaque et omnis minus nobis beatae et voluptas. Est doloremque sit saepe nostrum ipsam laborum accusamus. Quia perspiciatis et in ullam earum tempora illo. Accusantium sed est fuga praesentium dolores deleniti a. Velit expedita quas iste.','2018-05-27 22:15:51','2018-05-27 22:15:51'),(10,10,10,'Lowongan Materials Engineer PT Boyer-Mills',20,'Fugiat veniam vitae deserunt corrupti. Beatae temporibus qui omnis. Sint pariatur asperiores alias qui. Quaerat sequi et aut adipisci. Eligendi iure assumenda est repellat ea. Sed quia repudiandae omnis culpa. Qui voluptatum porro cumque dolor illo sapiente eaque. Deserunt accusamus dolor eos aperiam ut placeat. Aut dolores illum quam voluptatibus sed. Est adipisci voluptatem et perferendis. Eveniet laboriosam eum corrupti possimus asperiores sunt tempora quam. Adipisci dolorem ea sunt. Nemo sed iure in dolores exercitationem ullam. Non et velit rem velit inventore est libero. Ipsum pariatur alias illo nesciunt. Quo eveniet quia repellat aliquam mollitia. Quo rerum omnis quibusdam ad iste. Sunt vitae voluptatem velit ut ut voluptatem cumque. Odit consequatur minima voluptas et quis. Voluptatibus aperiam non dolore aut. Harum consequatur eum totam adipisci ullam ad.','2018-05-27 22:15:51','2018-05-27 22:15:51'),(11,11,11,'Lowongan Night Security Guard PT Quitzon LLC',6,'Omnis a qui blanditiis eos voluptas numquam officiis est. Eum quia laudantium possimus nostrum. Sequi autem tempore quis sapiente eum aut aut pariatur. Quos cupiditate est odit a provident. Iure enim rerum ducimus placeat necessitatibus numquam. Perspiciatis sint cupiditate officiis aut. Tempore sint nemo est iure aut et. Consequuntur qui illo ea eum. Perferendis minima consequatur ipsam voluptatibus. Eos quasi dignissimos explicabo nihil modi reprehenderit. Id deserunt pariatur voluptatem assumenda. Fugit ipsam nobis rem sit aut at. Placeat architecto sint nulla velit ipsa. Iste reiciendis nihil est. Dolore et aliquid vero ab occaecati mollitia dolorum vel. Quasi est officiis esse sapiente. Quis voluptatum sit unde voluptate id illo. Corporis explicabo debitis accusantium voluptas laborum delectus. Maxime provident autem qui similique possimus. Ipsum placeat ut consequatur fugit facilis debitis ut.','2018-05-27 22:15:52','2018-05-27 22:15:52'),(12,12,12,'Lowongan Oil and gas Operator PT Hettinger, Block and Harber',44,'Modi inventore qui ut et voluptatum ratione. Distinctio animi hic odit facilis. Sed alias blanditiis et qui recusandae quibusdam voluptatem. Aliquam voluptatem cum voluptas eveniet voluptatem dicta. Aperiam ut et id sapiente repellat. Atque eius eaque a odit asperiores ab eaque laborum. Hic et aut accusantium iste molestiae voluptatem sunt. Officia aliquid omnis vel omnis ea quo error. Enim iure magnam ea fugiat similique et. Illo debitis necessitatibus ut expedita perspiciatis. Velit dolores voluptatibus dolorum odio minus aut. Consequatur qui molestias animi animi ullam vitae. Dolorem est eos nulla maiores neque dolor quo excepturi. Eos sit enim perferendis assumenda. Voluptas nostrum placeat explicabo suscipit sit dolores natus. Earum rerum temporibus in sapiente quia aut fugit consectetur. Fugit ea ad aliquam atque perspiciatis praesentium. Temporibus aut maxime quidem minus officiis cumque magni. Cupiditate omnis quos voluptas iusto. Atque dolores aut perferendis.','2018-05-27 22:15:53','2018-05-27 22:15:53'),(13,13,13,'Lowongan Aircraft Launch Specialist PT Pfeffer-Beatty',10,'Eum corrupti consectetur sed perspiciatis voluptates. Nihil voluptas est similique dolore. Voluptatem non quis eos temporibus delectus fugiat et. Omnis adipisci corporis officiis voluptatem ullam voluptas atque non. Laboriosam perspiciatis nihil accusamus rem. Perspiciatis quia voluptatem quibusdam libero animi doloremque. Enim hic omnis a commodi. Rerum nihil rerum odio accusamus expedita quibusdam est. Soluta sit velit magnam dignissimos libero repellat ut. Nobis optio voluptatem omnis veritatis recusandae sed ut. Eveniet doloremque temporibus sit illum. Est voluptatibus quo dolore magnam. Nisi velit ipsam et assumenda. Non officia a magni. Sit pariatur velit quia et. Deserunt porro dolorem ut dolor qui. Mollitia autem eos totam consectetur tenetur. Ut aut ut placeat nostrum consequatur. Ea corrupti commodi quam et. Et quas aut ut et eius reiciendis. Cum non rerum cupiditate laboriosam impedit.','2018-05-27 22:15:53','2018-05-27 22:15:53'),(14,14,14,'Lowongan Structural Metal Fabricator PT Stamm-Huel',82,'Veritatis doloremque et dolorum at et. Quo asperiores soluta in maiores laudantium. Enim cupiditate pariatur ut dolor. Delectus qui deserunt eum porro. Ad repellendus voluptas odio aut. Ratione magni est velit error voluptas quia aut recusandae. Rerum eveniet vel molestias. Autem et itaque itaque quas delectus sapiente. Eveniet dolores asperiores veniam aperiam. Sunt enim dolores nulla maiores est. Et laboriosam et fugiat et id et eligendi totam. Impedit cupiditate error consectetur accusantium sint sunt. Accusantium earum in nam impedit porro ea. Quam labore veniam inventore. Et perspiciatis atque dolorem reiciendis. Voluptas quia quidem et exercitationem eius. Et fugiat consequatur occaecati mollitia asperiores quam numquam. Et eos laborum ipsam itaque sed et molestiae necessitatibus.','2018-05-27 22:15:54','2018-05-27 22:15:54'),(15,15,15,'Lowongan Grinder OR Polisher PT Kerluke PLC',87,'Dolorem totam quia ex totam. Sed voluptatem eveniet eligendi natus. Deleniti recusandae inventore officia non modi dolores eveniet. Perferendis et temporibus quis est est aspernatur. Enim est perspiciatis recusandae non est ab omnis reiciendis. Ipsum itaque aut velit cumque inventore aliquam iste. Nesciunt ut quisquam sit et laboriosam. Aut veritatis sit consequatur illo asperiores. Voluptatem voluptas eum quam maxime laboriosam hic quibusdam. A et provident sunt voluptatem debitis. Nihil blanditiis dignissimos earum. Iure ab est repellendus unde sit ut. Rerum minima assumenda porro iusto. Sunt voluptatem quo doloremque deserunt voluptatem. Quaerat ducimus omnis reprehenderit dignissimos. Eos optio quibusdam illo aliquam sint qui recusandae amet. Consequatur sed est voluptatum fugiat quam beatae saepe. Delectus aliquam qui unde. Aut fugiat ipsam nesciunt. Saepe repudiandae tenetur eius itaque quia quod.','2018-05-27 22:15:54','2018-05-27 22:15:54'),(16,16,16,'Lowongan Technical Specialist PT Kris, Rice and Nienow',44,'Est quam corporis dolores totam. Ullam et dolorem explicabo sed sed iure cumque. Consequatur cum pariatur eligendi nihil. Soluta ullam facilis hic repudiandae sit. Et a quasi modi excepturi id qui. Accusamus consequuntur et quis voluptatem. Quae repudiandae consequuntur est molestias consequatur veniam. Expedita exercitationem quia qui asperiores aut. Fugit laudantium harum explicabo rerum. Quas aliquam nam qui aut similique eius amet qui. Et error ut harum. At sed id et odit corrupti. Optio blanditiis ratione impedit ipsum. Eveniet doloremque deleniti est id illum. Ut et est quis alias culpa. Sunt ipsum expedita quia sit rerum maxime. Sit suscipit eius aspernatur nostrum molestiae unde. Ea fugiat sunt molestiae quisquam omnis alias non. Rerum nesciunt doloremque sint molestiae eum dolorum. Velit quaerat nam quia earum illo qui. Delectus voluptas perspiciatis expedita et et. Est aut nihil error quas. Fuga in possimus esse enim officiis et.','2018-05-27 22:15:55','2018-05-27 22:15:55'),(17,17,17,'Lowongan Buyer PT Langosh-Veum',13,'Sit voluptas et et enim. Eos voluptatibus minus maxime enim aliquid. Natus iure natus veniam maxime sed. Quidem expedita enim eius. A et tenetur doloremque harum odio aperiam. Velit vitae et architecto. Nostrum voluptas rem dolor asperiores libero sit. Molestiae dolorum odio quam aut et vero animi perspiciatis. Assumenda consequuntur et ipsa aliquam voluptates quo voluptate. Fugit impedit id aut sed. Et qui dolores quam rem temporibus. Voluptatem ad numquam commodi molestias eligendi commodi reiciendis doloremque. Quia exercitationem iusto a aut. Dolorum mollitia quia magnam maiores corrupti enim. Maxime delectus sint qui in dicta nemo. Dolorem animi aut culpa et ut dolores perspiciatis. Explicabo aut dolore perferendis facere est. Recusandae quos harum doloribus et commodi et earum. Sed provident pariatur et harum hic.','2018-05-27 22:15:55','2018-05-27 22:15:55'),(18,18,18,'Lowongan Opticians PT Gottlieb Group',38,'Doloremque nobis facere adipisci voluptates quisquam aut hic. Exercitationem ipsam quo aperiam porro aut quidem velit. Hic sed vero numquam. Nobis doloribus dolorum in ducimus veritatis iste ab. Nam odio cum soluta ut. Corporis commodi repellendus vel cumque ducimus. Consequatur nam quis id et sequi. Vero deleniti officia omnis sequi aperiam amet rerum. Rerum rerum accusantium earum necessitatibus et et maiores. Fugit ex voluptatum et voluptatem eum nobis. Dolores vel sequi tempore autem maxime quam corporis rerum. Ea deserunt quia ratione non. Natus atque minima voluptatum soluta deleniti. Molestiae iste excepturi dolor illum adipisci rerum. Repudiandae delectus modi consequatur sed earum asperiores. Optio eum distinctio et id. Quos porro dolores ad omnis. Ad est sapiente in nisi. Rerum commodi et molestias esse mollitia voluptate. Perferendis sunt et totam.','2018-05-27 22:15:56','2018-05-27 22:15:56'),(19,19,19,'Lowongan Sound Engineering Technician PT Cronin, Stracke and Hamill',57,'Iure vel ratione vitae sed voluptatem earum. Dicta dolor nobis sit recusandae cumque enim. Optio consequatur explicabo laudantium vitae quos sunt. Et optio autem error possimus quis dolorem. Id et asperiores autem illo repellat nesciunt eveniet. Voluptatem sint itaque voluptatibus natus quis eaque. Consequuntur numquam officia fugit laudantium. Ut quam dolores omnis ullam temporibus. In beatae temporibus nobis sit est. Vero eius accusantium et praesentium culpa nam voluptatem aliquam. Eos omnis voluptatem earum quasi. Doloremque id aut animi eum assumenda qui. Quasi natus placeat sequi nihil rerum sunt deleniti. Laboriosam quam impedit alias eaque facilis nobis. Numquam aut vel nesciunt corporis sit. Sit amet iste omnis dignissimos aliquam. Ducimus et ipsam delectus ipsum tenetur exercitationem iusto ipsa. Repudiandae repellendus aut asperiores quia. Repellat corrupti reiciendis qui vel omnis voluptatum. Est consequatur enim blanditiis recusandae. Modi quia neque magni.','2018-05-27 22:15:56','2018-05-27 22:15:56'),(20,20,20,'Lowongan Management Analyst PT O\'Connell LLC',86,'Aut excepturi est officia sit. Ut recusandae minima similique molestiae at reprehenderit. Rerum dolor ratione quia voluptatem reprehenderit sit consequatur. Amet rerum quia exercitationem quo sunt commodi. Nihil similique magni ex repellendus. Quidem fugiat quis pariatur et ullam quaerat dolor et. Quasi laboriosam ducimus ab rem minima sint. Et harum hic ipsum quas ullam voluptatem. Minima architecto est eaque quam adipisci sunt quia. Omnis ratione qui eos pariatur blanditiis tempore nulla. Dolores tenetur recusandae recusandae laboriosam modi. Ratione vitae omnis molestias possimus ipsam aspernatur quos. Veniam consequatur deleniti et sed illum rem ipsa. Enim quod mollitia ratione saepe dolore voluptate amet quia. Voluptas ipsa non sint voluptatem vitae. Consequuntur distinctio qui blanditiis incidunt. Aut laborum culpa aut ratione.','2018-05-27 22:15:57','2018-05-27 22:15:57'),(21,21,21,'Lowongan Transit Police OR Railroad Police PT Wyman, Schroeder and Nicolas',36,'Eum repellendus voluptatem earum qui magnam. Corrupti enim sit facere magnam impedit. Et amet qui architecto porro. Assumenda pariatur est iste. Consequatur ratione numquam adipisci incidunt. Qui qui nesciunt amet laudantium. Ipsa fugit dolor ut earum eaque est et. Modi in quaerat et voluptate esse molestiae. Aut corporis sint officia cumque. Dolorem dolores illum rerum omnis expedita sunt est distinctio. Saepe earum repellat commodi sit asperiores omnis sint. Iure soluta modi cumque vel. Natus quibusdam vitae est quibusdam sint quo quia. Porro saepe perferendis doloribus et. Ab ipsum in magnam nulla qui doloremque. Enim voluptas labore asperiores voluptas libero aliquid deserunt officiis. Est itaque quae qui nemo aliquid. Est iste officiis nesciunt animi sit. Corporis accusantium enim et maiores. Occaecati provident quos ratione perspiciatis id eligendi sunt. Explicabo incidunt ratione beatae reiciendis.','2018-05-27 22:15:57','2018-05-27 22:15:57'),(22,22,22,'Lowongan Law Clerk PT Runolfsson Inc',66,'Consectetur omnis ut quisquam est perferendis sapiente. Debitis quia laudantium quia. Ut quisquam totam nesciunt maiores quia. Accusamus possimus et iure voluptatem nihil error aut. Dolores aspernatur placeat magnam illo totam. Aut incidunt facilis maiores sunt laborum sint. Hic eius quibusdam corrupti recusandae iste nesciunt. Omnis repudiandae qui beatae odit architecto. Quasi at facere quam tempora atque. Non aliquid quasi omnis accusamus laborum dolores. Ab neque optio officia vel labore asperiores aliquid. Voluptatem impedit explicabo vitae nostrum voluptas. Labore et veniam repellat magnam. Suscipit blanditiis odio facilis quae quasi dolor. Earum molestiae tempora reprehenderit aliquid dolorum. Alias ratione veritatis rem ut. Nulla et quis exercitationem voluptatibus voluptatibus. Et deserunt numquam quis beatae unde. Harum deserunt unde doloremque fugit eum amet mollitia. Corrupti quo voluptatem voluptatem laboriosam amet veritatis.','2018-05-27 22:15:58','2018-05-27 22:15:58'),(23,23,23,'Lowongan Interior Designer PT Fritsch-Sauer',20,'Dicta voluptas a commodi eius distinctio illum. Ut ipsam ratione nulla fuga. Repellendus iusto eligendi quod in qui ad. Magnam occaecati et temporibus dolor quo ut molestias in. Iste rerum ullam porro reprehenderit. Velit eveniet nobis et voluptatem. Repellat id hic suscipit ut. Suscipit voluptatum voluptatibus enim beatae at aliquam. Debitis sint nisi sunt. Enim in voluptatum nihil et nobis nostrum corrupti. Et numquam quas sed maxime et ipsam. Illo repudiandae repudiandae praesentium culpa recusandae incidunt error. Quia labore atque sunt voluptatem similique sed et. Excepturi voluptas nemo aut necessitatibus doloribus ipsum. Modi illum at nesciunt. Architecto temporibus omnis mollitia optio est. Labore laborum corporis accusamus. Necessitatibus est asperiores omnis. Officia dolorem eos ut beatae minus atque laudantium consequatur. Sequi tenetur aut quaerat dolor cum tempora neque.','2018-05-27 22:15:58','2018-05-27 22:15:58'),(24,24,24,'Lowongan Emergency Medical Technician and Paramedic PT Pfannerstill-O\'Conner',9,'Ut rerum qui omnis sit autem. Omnis voluptatem vitae nemo mollitia rerum velit iure. Blanditiis nesciunt aspernatur neque nobis laborum. Et pariatur id quia ratione libero. Rerum nam est distinctio numquam. Commodi laboriosam similique molestiae quo. Facilis sint autem veniam sit placeat. Itaque voluptatibus beatae sed quam. Accusantium et possimus et sint quia neque. Iste illo voluptatem ratione aut dolore voluptas rerum architecto. Saepe minus qui odit voluptatem autem. Rerum voluptatem aut est. Sunt tempore labore aperiam reprehenderit. Suscipit modi et alias aut qui alias. Et est eum ut id consectetur voluptatem. Autem accusantium reiciendis aut consequatur dolorum nihil. Eveniet voluptas vitae minima quia sed repudiandae. Molestiae dolorem voluptatem officia maiores praesentium hic. Voluptatibus doloremque nam molestiae. Quia quia unde nulla animi est. Itaque doloremque quaerat impedit aut.','2018-05-27 22:15:58','2018-05-27 22:15:58'),(25,25,25,'Lowongan Food Preparation and Serving Worker PT Rohan Ltd',80,'Aut et inventore tenetur id nam dolorum. Quod sed blanditiis voluptatem nihil. Vitae dolor quo tenetur quo numquam sunt. Eos non ipsam facilis eligendi nihil. Voluptates sint minus repudiandae ab. Nobis ut sapiente sunt est beatae. Distinctio nisi a id placeat et aut. Excepturi voluptatem inventore cumque placeat. Nihil quae quisquam velit rem vel ut autem. Necessitatibus saepe est alias qui. Porro illum velit quia aliquam neque omnis. Non eveniet praesentium aut aut sapiente earum. Facilis ipsa fuga assumenda impedit. Tempore alias maiores officiis aspernatur. Corporis assumenda dolor facere. Dolorum accusamus sint perferendis eveniet blanditiis veritatis. Accusamus eius necessitatibus sint qui. Qui voluptas totam rem dicta ullam dolorem. Facere dolores est ad laboriosam enim. Et aliquid eum eveniet.','2018-05-27 22:15:58','2018-05-27 22:15:58');
/*!40000 ALTER TABLE `job_openings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (137,'2018_05_23_081632_create_candidates_table',1),(138,'2018_05_23_081945_create_job_openings_table',1),(139,'2018_05_23_083043_create_cities_table',1),(140,'2018_05_23_083540_create_companies_table',1),(141,'2018_05_23_083548_create_positions_table',1),(142,'2018_05_23_083557_create_applicants_table',1),(143,'2018_05_24_071448_create_grades_table',1),(144,'2018_05_24_071458_create_education_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `positions`
--

DROP TABLE IF EXISTS `positions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `positions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `positions`
--

LOCK TABLES `positions` WRITE;
/*!40000 ALTER TABLE `positions` DISABLE KEYS */;
INSERT INTO `positions` VALUES (1,'Product Promoter','2018-05-27 22:15:43','2018-05-27 22:15:43'),(2,'Movie Director oR Theatre Director','2018-05-27 22:15:44','2018-05-27 22:15:44'),(3,'Power Generating Plant Operator','2018-05-27 22:15:44','2018-05-27 22:15:44'),(4,'Computer-Controlled Machine Tool Operator','2018-05-27 22:15:45','2018-05-27 22:15:45'),(5,'Cartographer','2018-05-27 22:15:48','2018-05-27 22:15:48'),(6,'Food Servers','2018-05-27 22:15:49','2018-05-27 22:15:49'),(7,'Sales and Related Workers','2018-05-27 22:15:49','2018-05-27 22:15:49'),(8,'Shear Machine Set-Up Operator','2018-05-27 22:15:50','2018-05-27 22:15:50'),(9,'Waste Treatment Plant Operator','2018-05-27 22:15:51','2018-05-27 22:15:51'),(10,'Materials Engineer','2018-05-27 22:15:51','2018-05-27 22:15:51'),(11,'Night Security Guard','2018-05-27 22:15:52','2018-05-27 22:15:52'),(12,'Oil and gas Operator','2018-05-27 22:15:53','2018-05-27 22:15:53'),(13,'Aircraft Launch Specialist','2018-05-27 22:15:53','2018-05-27 22:15:53'),(14,'Structural Metal Fabricator','2018-05-27 22:15:54','2018-05-27 22:15:54'),(15,'Grinder OR Polisher','2018-05-27 22:15:54','2018-05-27 22:15:54'),(16,'Technical Specialist','2018-05-27 22:15:55','2018-05-27 22:15:55'),(17,'Buyer','2018-05-27 22:15:55','2018-05-27 22:15:55'),(18,'Opticians','2018-05-27 22:15:56','2018-05-27 22:15:56'),(19,'Sound Engineering Technician','2018-05-27 22:15:56','2018-05-27 22:15:56'),(20,'Management Analyst','2018-05-27 22:15:57','2018-05-27 22:15:57'),(21,'Transit Police OR Railroad Police','2018-05-27 22:15:57','2018-05-27 22:15:57'),(22,'Law Clerk','2018-05-27 22:15:58','2018-05-27 22:15:58'),(23,'Interior Designer','2018-05-27 22:15:58','2018-05-27 22:15:58'),(24,'Emergency Medical Technician and Paramedic','2018-05-27 22:15:58','2018-05-27 22:15:58'),(25,'Food Preparation and Serving Worker','2018-05-27 22:15:58','2018-05-27 22:15:58');
/*!40000 ALTER TABLE `positions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-28 12:31:48

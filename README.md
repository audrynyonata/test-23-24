#### test-23-24
# Project 23 - 24

Buat object **Kandidat** dan **Lowongan Kerja**. Setiap kandidat dapat menjadi **Pelamar** di satu atau lebih Lowongan Kerja. Fokus pada membuat **screen Lowongan Kerja**. Data Kandidat, Kota, Tingkat Pendidikan, Nilai Pendidikan dan Posisi gunakan dummy data.

## Kandidat
| Attributes    | Type    | Remark                |
| ------------- | ------- | --------------------- |
| Nama          | String  | Nama lengkap kandidat |
| Tempat lahir  | **Kota**  | Kota kelahiran        |
| Tanggal lahir | Date    | Tanggal lahir         |
| Usia          | Integer | Usia (computed)       |
| Pendidikan    | **Tingkat Pendidikan** | Tingkat pendidikan terakhir dan tahun |
| Nilai         | **Nilai Pendidikan** | Ilmu dan nilai yang diperoleh (nilai bisa dengan angka flot/karakter). |

## Lowongan Kerja
| Attributes    | Type    | Remark                |
| ------------- | ------- | --------------------- |
| Nama          | String  | Lowongan kerja        |
| Perusahaan    | **Perusahaan** | Nama perusahaan, alias (bila ada diganti nama perusahaan) |
| Posisi        | **Posisi** | Nama posisi (tidak boleh ada yang double) |
| Jumlah Posisi | Integer    | Jumlah orang yang dibutuhkan. |
| Keterangan    | String     | Keterangan        |
| Pelamar       | **Pelamar** | Nama **kandidat**, tanggal melamar, status (diterima/tidak) |


<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Home
Route::get('/', function () {
    return view('welcome');
});

// Jobs
Route::get('/jobs', 'JobsController@index')->name('jobs.index');
Route::get('/jobs/getjobs', 'JobsController@getJobs')->name('jobs.getjobs');
Route::get('/jobs/{job}', 'JobsController@show')->name('jobs.show');

// Candidates
Route::get('/candidates', 'CandidatesController@index')->name('candidates.index');
Route::get('/candidates/getcandidates', 'CandidatesController@getCandidates')->name('candidates.getcandidates');
Route::get('/candidates/create', 'CandidatesController@create')->name('candidates.create');
Route::get('/candidates/{candidate}', 'CandidatesController@show')->name('candidates.show');
Route::post('/candidates', 'CandidatesController@store')->name('candidates.store');
Route::get('/candidates/{candidate}/edit', 'CandidatesController@edit')->name('candidates.edit');
Route::patch('/candidates/{candidate}', 'CandidatesController@update')->name('candidates.update');
Route::delete('/candidates/{candidate}', 'CandidatesController@destroy')->name('candidates.destroy');

// Profile
Route::get('/profile', 'ProfileController@index')->name('profile.index');

// Balance
Route::get('/balance', function() {
	$options['secret_api_key'] = 'xnd_development_NImEfL4l0eT9wJY7d7UaGzHDYtemqdMpkHO0+Rxh9WXf+LanDgZ+gQ=='; 
	$xenditPHPClient = new XenditClient\XenditPHPClient($options); 
	$response = $xenditPHPClient->getBalance();
	return view('balance.index', compact('response'));
})->name('balance.index');

// Memberships
Route::post('/api/membership', 'MembershipsController@activate')->name('memberships.activate');
Route::get('/invoices/{id}', 'MembershipsController@getInvoice')->name('memberships.getinvoice');
Route::get('/candidates/{candidate}/memberships', 'MembershipsController@index')->name('memberships.index');
Route::post('/memberships', 'MembershipsController@store')->name('memberships.store');
Route::get('/memberships/{membership}', 'MembershipsController@show')->name('memberships.show');

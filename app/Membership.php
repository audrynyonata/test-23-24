<?php

namespace App;

use App\Candidate;
use Illuminate\Database\Eloquent\Model;

class Membership extends Model
{
	protected $fillable = ['candidate_id', 'membership_number', 'date_start', 'date_end', 'repeat', 'payment_id', 'payment_method', 'bank_code', 'retail_outlet_name', 'amount', 'paid_amount', 'fees_paid_amount', 'status'];
    public function candidate(){
    	return $this->belongsTo(Candidate::class);
    }
}

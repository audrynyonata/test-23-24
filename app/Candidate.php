<?php

namespace App;

use App\City;
use App\Applicants;
use App\Education;
use App\Grade;
use App\Membership;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    protected $fillable = ['name', 'city_id', 'date_birth', 'status'];

    public function city(){
    	return $this->belongsTo(City::class);
    }

    public function applicants(){
    	return $this->hasMany(Applicant::class);
    }
	
	public function education(){
    	return $this->hasMany(Education::class);
    }

	public function grades(){
    	return $this->hasMany(Grade::class);
    }

    // eloquent mutators
    public function setDateBirthAttribute($value){
        $this->attributes['date_birth'] = $value;
        $this->attributes['age']=Carbon::parse($value)->age;
    }

    public function memberships(){
        return $this->hasMany(Membership::class);
    }

}

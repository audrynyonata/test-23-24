<?php

namespace App\Http\Controllers;

use App\Job;
use DataTables;
use Illuminate\Http\Request;

class JobsController extends Controller
{
    public function index(){
    	return view('jobs.index');
    }

    public function show(Job $job){
    	$job->load('company')->load('position')->load('applicants');
    	return view('jobs.show',compact('job'));
    }

    public function getJobs()
    {
        $model = Job::with(['company','position']);
        return DataTables::eloquent($model)
            ->filter(function ($query) {
                        $query->where('valid_date', '>', now());
                    })
            ->addColumn('company', function (Job $job) {
                    return $job->company->name; })
            ->addColumn('position', function (Job $job) {
                    return $job->position->title; })
            ->make(true);
    }
}

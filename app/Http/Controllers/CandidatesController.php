<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\Applicant;
use App\Education;
use App\Grade;
use App\City;
use DataTables;
use Illuminate\Http\Request;

class CandidatesController extends Controller
{
    public function index(){
    	return view('candidates.index');
    }

    public function show(Candidate $candidate){
    	$candidate->load('city')->load('education')->load('grades')->load('applicants');
    	return view('candidates.show', compact('candidate'));
    }

	public function destroy(Candidate $candidate){
        Grade::where('candidate_id', '=', $candidate->id)->delete();
        Education::where('candidate_id', '=', $candidate->id)->delete();
        Applicant::where('candidate_id', '=', $candidate->id)->delete();
        $candidate->delete();
    	return redirect()->route('candidates.index');
    }

    public function create(){
        return view('candidates.create');
    }

    public function createCityIfNotExists($name){
        $city = City::where('name', '=', $name)->first();
        if (is_null($city)){
            $city = new City(['name' => $name]);
            $city->save();
        }
        return $city;
    }

    public function store(Request $request){
        $city = $this->createCityIfNotExists($request->city);
        $candidate = new Candidate(array_merge($request->all(), ['city_id' => $city->id, 'status' => 'Free']));
        $candidate->save();
        return redirect()->route('candidates.show', $candidate->id);
    }

    public function edit(Candidate $candidate){
        return view('candidates.edit', compact('candidate'));
    }

    public function update(Request $request, Candidate $candidate){
        $city = $this->createCityIfNotExists($request->city);
        $candidate->update(array_merge($request->all(), ['city_id' => $city->id]));
        return redirect()->route('candidates.show', $candidate->id);
    }

    public function getCandidates()
    {
        $model = Candidate::with('city');
        return DataTables::eloquent($model)
            ->addColumn('city', function (Candidate $candidate) {
                    return $candidate->city->name; })
            ->make(true);        
    }
}

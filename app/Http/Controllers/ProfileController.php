<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Candidate;

class ProfileController extends Controller
{
    public function index(){
    	$candidate=Candidate::find(1);
    	return view('profile.index', compact('candidate'));
    }
}

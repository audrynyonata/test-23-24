<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\Membership;
use XenditClient;
use Illuminate\Http\Request;

class MembershipsController extends Controller
{
    public function store(Request $request){
        if (Membership::where('candidate_id', $request->candidate_id)->where('status','PENDING')->first()) {
            return redirect()->route('memberships.index', $request->candidate_id);
        }
    	$options['secret_api_key'] = 'xnd_development_NImEfL4l0eT9wJY7d7UaGzHDYtemqdMpkHO0+Rxh9WXf+LanDgZ+gQ=='; 
		$membership = new Membership([
			'candidate_id'=> $request->candidate_id,
			'repeat' => 0,			
			]);
		$membership->save(); //temporary

    	$xenditPHPClient = new XenditClient\XenditPHPClient($options); 
    	$external_id = (string)$membership->id;
		$amount = 150000;
		$payer_email = $request->payer_email;
		$description = 'Registration Fee ' . $payer_email;
		$response = $xenditPHPClient->createInvoice($external_id, $amount, $payer_email, $description);	

		$membership->update(array_merge($response, [
			'payment_id' => $response['id'],
			]));
        $request->session()->flash('new_tab', true);
		return redirect()->route('memberships.show', $membership);
    }

    public function index(Candidate $candidate){
		return view('memberships.index', compact('candidate'));
    }

    public function show(Membership $membership){
    	return view('memberships.show', compact('membership'));
    }

    public function activate(Request $request){
        $now = now();
    	$membership = Membership::find((int)$request->external_id);
        $repeat = Membership::where('candidate_id', $membership->candidate_id)->max('repeat');
    	$number = $now->year . $now->month . $membership->candidate_id . $membership->id; //temporary
    	$membership->update(array_merge($request->all(), [
    		'membership_number' => $number,
    		'date_start' => $now, 
    		'date_end' => $now->copy()->addDays(365)->subDays(1),
    		'repeat' => $repeat + 1,
		]));
//        $membership->candidate->update(['status' => 'Member']);
		return response('OK', 200)
        	->header('Content-Type', 'text/plain');
	}

    public function getInvoice($id){
        $options['secret_api_key'] = 'xnd_development_NImEfL4l0eT9wJY7d7UaGzHDYtemqdMpkHO0+Rxh9WXf+LanDgZ+gQ=='; 
        $xenditPHPClient = new XenditClient\XenditPHPClient($options);
        $response = $xenditPHPClient->getInvoice($id);
        return redirect($response['invoice_url']);
    }
}

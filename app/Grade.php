<?php

namespace App;

use App\Candidate;
use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    public function candidate(){
    	return $this->belongsTo(Candidate::class);
    }
}

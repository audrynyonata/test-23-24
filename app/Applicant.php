<?php

namespace App;
use App\Job;
use App\Candidate;

use Illuminate\Database\Eloquent\Model;

class Applicant extends Model
{
    public function job(){
    	return $this->belongsTo(Job::class);
    }

    public function candidate(){
    	return $this->belongsTo(Candidate::class);
    }
}

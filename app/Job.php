<?php

namespace App;

use App\Company;
use App\Position;
use App\Applicant;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    public function company(){
    	return $this->belongsTo(Company::class);
    }

    public function position(){
    	return $this->belongsTo(Position::class);
    }

    public function applicants(){
    	return $this->hasMany(Applicant::class);
    }
}

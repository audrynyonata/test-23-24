<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="/js/materialize.min.js" type="text/javascript"></script>
        <link href="/css/materialize.min.css" rel="stylesheet" type="text/css">
        @yield('header')
    </head>
    <body>
        <!-- Dropdown Structure -->
        <ul id="nav-dropdown" class="dropdown-content">
            <li class="nav-home"><a href="/">Home</a></li>
            <li class="nav-candidates"><a href="/candidates">Kandidat</a></li>
            <li class="nav-jobs"><a href="/jobs">Lowongan Kerja</a></li>
            <li class="nav-balance"><a href="/balance">Balance</a></li>
            <li class="nav-profile"><a href="/profile">Profil</a></li>
        </ul>
        <div class="navbar-fixed">
            <nav>
                <div class="nav-wrapper">
                    <a href="/" class="brand-logo">Lowongan Kerja</a>
                    <a class="right dropdown-trigger hide-on-large-only" href="#!" data-target="nav-dropdown">Menu &nbsp; &#9660; &nbsp;</a>
                    <ul class="right hide-on-med-and-down">
                        <li class="nav-home"><a href="/">Home</a></li>
                        <li class="nav-candidates"><a href="/candidates">Kandidat</a></li>
                        <li class="nav-jobs"><a href="/jobs">Lowongan Kerja</a></li>
                        <li class="nav-balance"><a href="/balance">Balance</a></li>
                        <li class="nav-profile"><a href="/profile">Profil</a></li>
                    </ul>
                </div>
            </nav>
        </div>
        <div style="padding:6px;">
            @yield('content')
        </div>
        <script>
            $(".dropdown-trigger").dropdown();
        </script>
        @yield('footer')
    </body>
</html>

@extends('layout')
@section('header')
    <title>Edit Kandidat</title>
@stop

@section('content')
    <div class="row valign-wrapper">
        <div class="col s12 m10">
            <h5>Edit Kandidat</h5>
        </div>
        <div class="col s12 m2">
            <div class="col right">
                <form id="form-delete" action="{{ route('candidates.destroy',$candidate->id) }}" method="post">
                    {{ method_field('DELETE') }}
                    {!! csrf_field() !!}
                    <a href="javascript: del('{{ $candidate->name }}');">Delete</a>      
                </form>
            </div>
       </div>
    </div>
    <div class="row">
        <div class="col m12 s12">
            <form action="{{ route('candidates.update',$candidate->id) }}" method="post">
                {{ method_field('PATCH') }}
                {!! csrf_field() !!}
                <div class="input-field">
                    <input disabled id="id" name="id" type="text" value="{{ $candidate->id }}">
                    <label for="id">ID</label>
                </div>
                <div class="input-field">
                    <input id="name" name="name" type="text" value="{{ $candidate->name }}">
                    <label for="name">Nama</label>
                </div>
                <div class="input-field">
                    <input id="city" name="city" type="text" value="{{ $candidate->city->name }}">
                    <label for="city">Tempat Lahir</label>
                </div>
                <div class="input-field">
                    <input id="birth_date" name="birth_date" type="text" class="datepicker" value="{{ $candidate->birth_date }}">
                    <label for="birth_date">Tanggal Lahir</label>
                </div>
                <div class="input-field">
                    <input readonly id="age" type="text" value="{{ $candidate->age }} tahun">
                    <label for="age">Usia</label>
                </div>
                <button type="submit" class="btn">Update</button>           
            </form>
        </div>
    </div>
@stop

@section('footer')
    <script>
        $(document).ready(function(){
            $('.nav-candidates').addClass('active');
            $('.datepicker').datepicker({
                format: 'yyyy-mm-dd',
                maxDate: new Date(),
            });
            M.updateTextFields();  
        });
      
        function del(name){
            if(confirm("Hapus kandidat "+ name +"?")){
                $('#form-delete').submit();    
            }
            else{
                return false;
            }
        }

        $('#birth_date').change(function() {
            var today = new Date();
            var dob = new Date($('#birth_date').val());
            $('#age').val((today.getFullYear() - dob.getFullYear()));
        });

    </script>
@stop   
@extends('layout')
@section('header')
    <title>Daftar Kandidat</title>
@stop

@section('content')
    <div class="row valign-wrapper">
        <div class="col s8 m6">
            <h5>Daftar Kandidat</h5>
        </div>
        <div class="col s4 m6">
            <a href="{{ route('candidates.create') }}" class="waves-effect waves-teal btn right">+ Tambah Kandidat</a>
        </div>
    </div>
    <div class="row">    
        <div class="col s12 m12">
            <table class="table datatable" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nama</th>
                        <th>Tempat Lahir</th>
                        <th>Tanggal Lahir</th>
                        <th>Usia</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>

            </table>
        </div>
    </div>
@stop

@section('footer')
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet">
    <script>
        $(document).ready(function(){
            $('.nav-candidates').addClass('active');
            $('.datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route('candidates.getcandidates') }}',
                columns: [
                    {data: 'id', name: 'id'},
                    {
                        data: 'name', 
                        name: 'name',
                        render: function ( data, type, row, meta ) {
                            return '<a href="/candidates/' + row.id + '">' + row.name + '</a>';
                        }
                    },
                    {data: 'city', name: 'city.name'},
                    {data: 'date_birth', name: 'date_birth'},
                    {data: 'age', name: 'age'},
                    {
                        data: 'action_edit', 
                        name:'action_edit', 
                        sortable: false,
                        searchable: false,
                        render: function ( data, type, row, meta ) {
                            return '<a href="/candidates/' + row.id + '/edit">Edit</a>';
                        }
                    },
                    {
                        data: 'action_delete', 
                        name:'action_delete', 
                        sortable: false,
                        searchable: false,
                        render: function ( data, type, row, meta ) {
                            return '<form id="form-delete' + row.id + '" action="/candidates/'+ row.id +'" method="post">' +
                                '{{ method_field("DELETE") }}' +
                                '{{ csrf_field() }}' +
                            '<a href="javascript: del(' + row.id + ',' + "'" + row.name+ "'" + ');">Delete</a>' +      
                            '</form>';
                        }
                    },
                ]
            });
            $('select').formSelect();
            
        });

        function del(id, name){
            if(confirm("Hapus kandidat "+ name +"?")){
                $('#form-delete' + id).submit();    
            }
            else{
                return false;
            }
        }
    </script>
@stop
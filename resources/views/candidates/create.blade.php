@extends('layout')
@section('header')
    <title>Tambah Kandidat</title>
@stop

@section('content')
    <div class="row valign-wrapper">
        <div class="col s12 m12">
            <h5>Tambah Kandidat</h5>
        </div>
    </div>
    <div class="row">
        <div class="col m12 s12">
            <form action="{{ route('candidates.store') }}" method="post">
                {!! csrf_field() !!}
                <div class="input-field">
                    <input id="name" name="name" type="text">
                    <label for="name">Nama</label>
                </div>
                <div class="input-field">
                    <input id="city" name="city" type="text">
                    <label for="city">Tempat Lahir</label>
                </div>
                <div class="input-field">
                    <input id="birth_date" name="birth_date" type="text" class="datepicker">
                    <label for="birth_date">Tanggal Lahir</label>
                </div>
                <div class="input-field">
                    <input readonly id="age" type="text" value="0">
                    <label for="age">Usia</label>
                </div>
                <button type="submit" class="btn">Create</button>           
            </form>
        </div>
    </div>
@stop

@section('footer')
    <script>
        var today = new Date();
        $(document).ready(function(){
            $('.nav-candidates').addClass('active');
            $('.datepicker').datepicker({
                format: 'yyyy-mm-dd',
                maxDate: today,
            }).val(today.toISOString().split('T')[0]);
            M.updateTextFields();  
        });
      
        function del(name){
            if(confirm("Hapus kandidat "+ name +"?")){
                $('#form-delete').submit();    
            }
            else{
                return false;
            }
        }

        $('#birth_date').change(function() {
            var today = new Date();
            var dob = new Date($('#birth_date').val());
            $('#age').val((today.getFullYear() - dob.getFullYear()));
        });

    </script>
@stop   
@extends('layout')
@section('header')
    <title>Lowongan Kerja</title>
@stop

@section('content')
    <div class="row">
        <div class="col s12 m3">
          <div class="card small">
            <div class="card-content">
              <span class="card-title">Kandidat</span>
              <p>
              <ul>
                <li>nama, kota kelahiran, tanggal lahir, usia, riwayat pendidikan dan nilai.</li>
              </ul>
              </p>
            </div>
            <div class="card-action">
              <a href="{{ route('candidates.index') }}">Daftar Kandidat</a>
            </div>
          </div>
        </div>
        <div class="col s12 m3">
          <div class="card small">
            <div class="card-content">
              <span class="card-title">Lowongan Kerja</span>
              <p>
              <ul>
              <li>lowongan, nama perusahaan, posisi, jumlah posisi, keterangan, dan daftar pelamar.</li></p>
            </div>
            <div class="card-action">
                <a href="{{ route('jobs.index') }}">Daftar Lowongan Kerja</a>
            </div>
          </div>
        </div>
        <div class="col s12 m3">
          <div class="card small">
            <div class="card-content">
              <span class="card-title">Balance</span>
              <p>
              <ul>
              <li>using XenditPHPClient</li></p>
            </div>
            <div class="card-action">
                <a href="{{ route('balance.index') }}">Show Balance</a>
            </div>
          </div>
        </div>
        <div class="col s12 m3">
          <div class="card small">
            <div class="card-content">
              <span class="card-title">Profil</span>
              <p>
              <ul>
              <li>data diri, pembayaran</li></p>
            </div>
            <div class="card-action">
                <a href="{{ route('profile.index') }}">Lihat Profil</a>
            </div>
          </div>
        </div>
    </div>
@stop

@section('footer')
    <script>
        $(document).ready(function(){
            $('#nav-home').addClass('active');
        });
    </script>
@stop
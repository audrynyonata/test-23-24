@extends('layout')
@section('header')
    <title>Balance</title>
@stop

@section('content')
    <div class="row">
        <div class="col s12 m10">
            <h5>Balance</h5>
        </div>
    </div>

    <div class="row">
        <div class="col s6 m2">
            Current balance
        </div>
        <div class="col s6 m10">
            {{$response['balance']}}
        </div>
    </div>
@stop

@section('footer')
    <script>
        $(document).ready(function(){
            $('.nav-balance').addClass('active');
        });
    </script>
@stop
@extends('layout')
@section('header')
    <title>{{ $job->name }}</title>
@stop

@section('content')
    <div class="row">
        <div class="col s12 m12">
            <h5>{{ $job->name }}</h5>
        </div>
        <div class="col s6 m2">
            ID
        </div>
        <div class="col s6 m10">
            {{$job->id}}
        </div>
        <div class="col s6 m2">
            Nama
        </div>
        <div class="col s6 m10">
            {{$job->name}}
        </div>
        <div class="col s6 m2">
            Perusahaan
        </div>
        <div class="col s6 m10">
            {{$job->company->name}}
        </div>    
        <div class="col s6 m2">
            Posisi
        </div>
        <div class="col s6 m10">
            {{$job->position->title}}
        </div>    
        <div class="col s6 m2">
            Jumlah Posisi
        </div>
        <div class="col s6 m10">
            {{$job->quota}}
        </div>    
        <div class="col s6 m2">
            Keterangan
        </div>
        <div class="col s6 m10">
            {{$job->description}}
        </div>    
    </div>
    <div class="row">
        <div class="col s12 m12">
            <h5>Pelamar</h5>
            <table class="highlight">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nama</th>
                    <th>Tanggal Melamar</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @if ($job->applicants->isEmpty())
                    <tr>
                        <td colspan='4'>Tidak ada pelamar
                        </td>
                    <tr>
                @else
                    @foreach($job->applicants as $applicant)
                            <tr>
                                <td><a href="/candidates/{{ $applicant->candidate->id }}">{{ $applicant->id }}</a></td>
                                <td><a href="/candidates/{{ $applicant->candidate->id }}">{{ $applicant->candidate->name }}</a></td>
                                <td>{{ $applicant->date }}</td>
                                <td>{{ $applicant->status }}</td>
                            </tr>
                    @endforeach
                @endif    
            </tbody>
            </table>
        </div>      
    </div>
@stop

@section('footer')
    <script>
        $(document).ready(function(){
            $('.nav-jobs').addClass('active');
        });
    </script>
@stop
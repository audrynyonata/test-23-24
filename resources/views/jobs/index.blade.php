@extends('layout')
@section('header')
    <title>Daftar Lowongan Kerja</title>
@stop

@section('content')
    <div class="row">
        <div class="col s12 m12">
            <h5>Daftar Lowongan Kerja</h5>
            <table class="table datatable" style="width:100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nama</th>
                    <th>Perusahaan</th>
                    <th>Posisi</th>
                    <th>Jumlah Posisi</th>
                    <th>Berlaku sampai</th>
                </tr>
            </thead>

            </table>
        </div>      
    </div>
@stop

@section('footer')
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet">
    <script>
        $(document).ready(function(){
            $('.nav-jobs').addClass('active');
            $('.datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route('jobs.getjobs') }}',
                columns: [
                    {data: 'id', name: 'id'},
                    {
                        data: 'name', 
                        name: 'name', 
                        render: 
                            function ( data, type, row, meta ) {
                                return '<a href="/jobs/' + row.id + '">' + row.name + '</a>';
                        }
                    },
                    {data: 'company', name: 'company.name'},
                    {data: 'position', name: 'position.title'},
                    {data: 'quota', name: 'quota'},
                    {data: 'valid_date', name: 'valid_date'},
                ]
            });
            $('select').formSelect();
        });

    </script>
@stop

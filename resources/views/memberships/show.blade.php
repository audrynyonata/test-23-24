@extends('layout')
@section('header')
    <title>Membership</title>
@stop

@section('content')
	<div class="row">
        <div class="col s12 m10">
            <h5>Membership</h5>
        </div>
    </div>

    <div class="row">
        <div class="col s6 m2">
            ID
        </div>
        <div class="col s6 m10">
            {{$membership->id}}
        </div>
        <div class="col s6 m2">
            Candidate ID
        </div>
        <div class="col s6 m10">
            {{$membership->candidate_id}}
        </div>
        <div class="col s6 m2">
            Membership Number
        </div>
        <div class="col s6 m10">
            {{$membership->membership_number}}&nbsp;
        </div>
        <div class="col s6 m2">
            Start Date
        </div>
        <div class="col s6 m10">
            {{$membership->date_start}}&nbsp;
        </div>
        <div class="col s6 m2">
            End Date
        </div>
        <div class="col s6 m10">
            {{$membership->date_end}}&nbsp;
        </div>
        <div class="col s6 m2">
            Repeat
        </div>
        <div class="col s6 m10">
            {{$membership->repeat}}
        </div>
        <div class="col s6 m2">
            Payment ID
        </div>
        <div class="col s6 m10">
            <a href="{{ route('memberships.getinvoice', $membership->payment_id) }}" target="_blank">{{$membership->payment_id}}</a>
        </div>
        <div class="col s6 m2">
            Payment Method
        </div>
        <div class="col s6 m10">
            {{$membership->payment_method}}&nbsp;
        </div>
        <div class="col s6 m2">
            Bank Code
        </div>
        <div class="col s6 m10">
            {{$membership->bank_code}}&nbsp;
        </div>
        <div class="col s6 m2">
            Retail Outlet Name
        </div>
        <div class="col s6 m10">
            {{$membership->retail_outlet_name}}&nbsp;
        </div>
        <div class="col s6 m2">
            Amount
        </div>
        <div class="col s6 m10">
            {{$membership->amount}}&nbsp;
        </div>        
        <div class="col s6 m2">
            Paid Amount
        </div>
        <div class="col s6 m10">
            {{$membership->paid_amount}}&nbsp;
        </div>        
        <div class="col s6 m2">
            Fees Paid Amount
        </div>
        <div class="col s6 m10">
            {{$membership->fees_paid_amount}}&nbsp;
        </div>        
        <div class="col s6 m2">
            Status
        </div>
        <div class="col s6 m10">
            {{$membership->status}}
        </div>
    </div>
@stop

@section('footer')
    <script>
    	@if (session('new_tab'))
		    window.open("{{ route('memberships.getinvoice', $membership->payment_id) }}", '_blank');
	    @endif
	    $(document).ready(function(){
            $('.nav-profile').addClass('active');
        });
    </script>
@stop
@extends('layout')

@section('header')
	<title>Invoice</title>
@stop

@section('content')
	<div class="row center">
		<div class="col s12 m12">
			<table>
				<tr>
					<th>ID</th>
					<th>No Membership</th>
					<th>Payment ID</th>
					<th>Tanggal</th>
					<th>Tanggal Berakhir</th>
					<th>Status Invoice</th>
					<th>Repeat</th>
				</tr>			
				@if ($candidate->memberships->isEmpty())
					<tr>
						<td colspan='7'>
							Tidak ada keanggotaan. 
						</td>
					</tr>
				@else
					@foreach($candidate->memberships as $membership) 
						<tr>
							<td> <a href="{{ route('memberships.show', $membership) }}">{{ $membership->id }} </td>
							<td> <a href="{{ route('memberships.show', $membership) }}">{{ $membership->membership_number }}</a></td>
							<td> <a href="{{ route('memberships.getinvoice', $membership->payment_id) }}" target="_blank">{{ $membership->payment_id }}</a> </td>
							<td> {{ $membership->date_start }} </td>
							<td> {{ $membership->date_end }} </td>
							<td> {{ $membership->status }} </td>
							<td> {{ $membership->repeat }} </td>
						</tr>
					@endforeach
				@endif
			</table>
		</div>
	</div>	
	<div class="row">
		<div class="col s12 m4 offset-m4 center">
			<form id="form-invoice" action="{{ route('memberships.store') }}" method="post">
				{{ csrf_field() }}
				<input type="hidden" name="candidate_id" value="{{ $candidate->id}}">
				<div class="input-field">
			      <input id="email" type="email" name="payer_email" class="validate">
			      <label for="email">Email</label>
			    </div>
			    <button id="button-create-invoice" class="btn" type="submit">$ Bayar</button>
			</form>
		</div>
	</div>
@stop

@section('footer')
	<script> 
	    $(document).ready(function(){
            $('.nav-profile').addClass('active');
        });
//	    if (($candidate->status == 'Member') || !$candidate->memberships->where('status','PENDING')->isEmpty())
	    @if (!$candidate->memberships->where('status','PENDING')->isEmpty())
	    	$('#button-create-invoice').prop('disabled', true);
	    @endif
    </script>
@stop
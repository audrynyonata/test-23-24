@extends('layout')
@section('header')
    <title>Profil</title>
@stop

@section('content')
    <div class="row valign-wrapper">
        <div class="col s12 m8">
            <h5>{{ $candidate->name }}</h5>
        </div>
        <div class="col s12 m4" >
            <a class="btn right" href="{{ route('memberships.index', $candidate) }}">Memberships</a>
       </div>
    </div>
    <div class="row">
        <div class="col s6 m2">
            ID
        </div>
        <div class="col s6 m10">
            {{$candidate->id}}
        </div>
        <div class="col s6 m2">
            Nama
        </div>
        <div class="col s6 m10">
            {{$candidate->name}}
        </div>
        <div class="col s6 m2">
            Tempat Lahir
        </div>
        <div class="col s6 m10">
            {{$candidate->city->name}}
        </div>    
        <div class="col s6 m2">
            Tanggal Lahir
        </div>
        <div class="col s6 m10">
            {{$candidate->date_birth}}
        </div>    
        <div class="col s6 m2">
            Usia
        </div>
        <div class="col s6 m10">
            {{$candidate->age}} tahun
        </div>    
        <div class="col s6 m2">
            Status
        </div>
        <div class="col s6 m10">
            {{$candidate->status}}
        </div>       </div>
    <div class="row">
        <div class="col s12 m6">
            <h5>Pendidikan Terakhir</h5>
            <table class="striped">
                <tr>
                    <th>ID</th>
                    <th>Tingkat</th>
                    <th>Tahun</th>
                </tr>
                @if($candidate->education->isEmpty())
                    <tr>
                        <td colspan='3'>Tidak ada pendidikan terakhir.</td>
                    </tr>
                @else
                    @foreach($candidate->education as $education)
                        <tr>
                            <td>{{ $education->id }}</td>
                            <td>{{ $education->level }}</td>
                            <td>{{ $education->year }}</td>
                        </tr>
                    @endforeach              
                @endif
            </table>
        </div>
        <div class="col s12 m6">    
            <h5>Nilai Pendidikan</h5>
            <table class="striped">
                <tr>
                    <th>ID</th>
                    <th>Ilmu</th>
                    <th>Nilai</th>
                </tr>
                @if($candidate->grades->isEmpty())
                    <tr>
                        <td colspan='3'>Tidak ada nilai pendidikan.</td>
                    </tr>
                @else
                    @foreach($candidate->grades as $grade)
                        <tr>
                            <td>{{ $grade->id }}</td>
                            <td>{{ $grade->subject }}</td>
                            <td>{{ $grade->mark }}</td>
                        </tr>
                    @endforeach              
                @endif
            </table>
        </div>
        <div class="col s12 m12">    
            <h5>Pelamar</h5>
            <table class="highlight">
                <tr>
                    <th>ID</th>
                    <th>Lowongan Kerja</th>
                    <th>Tanggal Melamar</th>
                    <th>Status</th>
                </tr>
                @if($candidate->applicants->isEmpty())
                    <tr>
                        <td colspan='4'>Tidak pernah melamar.</td>
                    </tr>
                @else
                    @foreach($candidate->applicants as $applicant)
                    <tr>
                        <td><a href="/jobs/{{ $applicant->job->id }}">{{ $applicant->id }}</a></td>
                        <td><a href="/jobs/{{ $applicant->job->id }}">{{ $applicant->job->name }}</a></td>
                        <td>{{ $applicant->date }}</td>
                        <td>{{ $applicant->status }}</td>
                    </tr>
                    @endforeach              
                @endif
            </table>
        </div>
    </div>
@stop

@section('footer')
    <script>
        $(document).ready(function(){
            $('.nav-profile').addClass('active');
        });

        function del(name){
            if(confirm("Hapus kandidat "+ name +"?")){
                $('#form-delete').submit();    
            }
            else{
                return false;
            }
        }
    </script>
@stop